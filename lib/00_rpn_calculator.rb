class RPNCalculator
  attr_accessor :stack, :value

  def initialize
    @stack = []
  end

  def push(number)
    stack << number
  end

  def plus
    perform_op(:+)
  end

  def minus
    perform_op(:-)
  end

  def times
    perform_op(:*)
  end

  def divide
    perform_op(:/)
  end

  def perform_op(operator)

    raise "calculator is empty" if @stack.count < 2

    right_op = stack.pop
    left_op = stack.pop

    case operator

    when :+
      @value = left_op + right_op
    when :-
      @value = left_op - right_op
    when :*
      @value = left_op * right_op
    when :/
      @value = left_op.fdiv(right_op)
    end
    stack << value
  end

  def tokens(string)
    operators = %w(+ - * /)

    string.split(" ").map do |el|
      if operators.include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(string)
    tokenize = tokens(string)

    tokenize.each do |token|

      stack << token if token.is_a?(Integer)

      if token.is_a?(Symbol)
        case token
        when :+
          plus
        when :-
          minus
        when :*
          times
        when :/
          divide
        end
      end
    end
      value

  end

end
